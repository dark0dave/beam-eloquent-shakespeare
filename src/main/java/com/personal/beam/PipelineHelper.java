package com.personal.beam;

import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.options.PipelineOptions;
import org.apache.beam.sdk.options.PipelineOptionsFactory;

class PipelineHelper {
  private static final PipelineOptions options = PipelineOptionsFactory.create();
  private Pipeline p = Pipeline.create(options);

  void set(Pipeline pipeline) {
    p = pipeline;
  }

  Pipeline get() {
    return p;
  }
}
