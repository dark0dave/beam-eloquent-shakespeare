package com.personal.beam;

import static com.personal.beam.Helpers.Mappers.flatMap;
import static com.personal.beam.Helpers.Mappers.map;
import static java.util.Arrays.stream;
import static org.apache.beam.sdk.io.TextIO.*;
import static org.apache.beam.sdk.values.TypeDescriptors.*;

import com.personal.beam.Helpers.GenericCoder;
import java.util.stream.Collectors;
import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.PipelineResult;
import org.apache.beam.sdk.transforms.*;
import org.apache.beam.sdk.values.KV;
import org.apache.beam.sdk.values.PCollection;

class Eloquent {
  public String filePath = System.getenv("SHAKESPEARE_PATH");

  static PCollection<KV<String, Long>> createHistogram(PCollection<String> inputString) {
    final PCollection<String> shakespeareWords =
        inputString
            .apply(
                "Split by word",
                flatMap(
                    (String sentence) ->
                        stream(sentence.toLowerCase().split("[^\\p{L}]+"))
                            .filter(word -> !word.isEmpty())
                            .collect(Collectors.toList())))
            .setCoder(new GenericCoder<>());

    return shakespeareWords
        .apply(
            "Count By Value",
            MapElements.into(kvs(strings(), longs()))
                .via((String element) -> KV.of(Integer.toString(element.length()), 1L)))
        .apply("Count By Key", Count.perKey());
  }

  static <T> MapElements<T, String> printToConsole() {
    return map(
        (T element) -> {
          System.out.println(element.toString());
          return element.toString();
        });
  }

  PipelineResult.State createAndPrintHistogram(PipelineHelper pipelineHelper) {
    final Pipeline p = pipelineHelper.get();
    final PCollection<String> shakespeare = p.apply("Read from file path", read().from(filePath));
    final PCollection<String> histogram =
        createHistogram(shakespeare)
            .apply("Write to console", printToConsole())
            .setCoder(new GenericCoder<>());
    histogram.apply("Write to file", write().to("histogram.txt"));

    return p.run().waitUntilFinish();
  }
}
