package com.personal.beam;

public class Runner {
  static Eloquent eloquent = new Eloquent();
  static PipelineHelper pipelineHelper = new PipelineHelper();

  static void setEloquent(Eloquent eloquent) {
    Runner.eloquent = eloquent;
  }

  static void setPipelineHelper(PipelineHelper pipelineHelper) {
    Runner.pipelineHelper = pipelineHelper;
  }

  public static void main(String[] args) {
    eloquent.createAndPrintHistogram(pipelineHelper);
  }
}
