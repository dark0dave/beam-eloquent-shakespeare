package com.personal.beam.Helpers;

import java.io.*;
import java.util.List;
import org.apache.beam.sdk.coders.Coder;

public final class GenericCoder<T> extends Coder<T> {
  @Override
  public void encode(final T value, final OutputStream outStream) throws IOException {
    final ByteArrayOutputStream bos = new ByteArrayOutputStream();
    final ObjectOutputStream oos = new ObjectOutputStream(bos);
    oos.writeObject(value);
    oos.flush();
    outStream.write(bos.toByteArray());
  }

  @Override
  public T decode(final InputStream inStream) {
    try {
      final ObjectInput in = new ObjectInputStream(inStream);
      return (T) in.readObject();
    } catch (ClassNotFoundException | IOException e) {
      throw new IllegalStateException(e);
    }
  }

  @Override
  public List<? extends Coder<?>> getCoderArguments() {
    return null;
  }

  @Override
  public void verifyDeterministic() {}
}
