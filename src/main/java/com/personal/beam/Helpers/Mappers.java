package com.personal.beam.Helpers;

import java.lang.reflect.Type;
import org.apache.beam.sdk.transforms.FlatMapElements;
import org.apache.beam.sdk.transforms.MapElements;
import org.apache.beam.sdk.transforms.ProcessFunction;
import org.apache.beam.sdk.values.TypeDescriptor;
import org.apache.beam.sdk.values.TypeDescriptors;

public final class Mappers {
  private Mappers() {}

  public static <InputType, OutputType> MapElements<InputType, OutputType> map(
      final ProcessFunction<InputType, OutputType> fn) {
    return MapElements.into(TypeDescriptors.outputOf(fn)).via(fn);
  }

  public static <out, input> FlatMapElements<input, out> flatMap(
      ProcessFunction<input, Iterable<out>> fn) {
    return FlatMapElements.into(
            new TypeDescriptor<out>() {
              @Override
              public Type getType() {
                return super.getType();
              }
            })
        .via(fn);
  }
}
