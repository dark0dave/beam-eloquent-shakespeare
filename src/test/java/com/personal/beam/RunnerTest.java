package com.personal.beam;

import static org.apache.beam.sdk.PipelineResult.State.DONE;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import java.io.Serializable;
import org.apache.beam.sdk.PipelineResult.State;
import org.junit.Test;

public class RunnerTest implements Serializable {
  @Test
  public void testMain() {
    final Eloquent eloquentMock = mock(Eloquent.class);
    when(eloquentMock.createAndPrintHistogram(any())).thenReturn(DONE);
    final PipelineHelper pipelineHelper = mock(PipelineHelper.class);

    final Runner run = new Runner();
    run.setEloquent(eloquentMock);
    run.setPipelineHelper(pipelineHelper);

    run.main(new String[] {});

    final State result = eloquentMock.createAndPrintHistogram(pipelineHelper);
    assertThat(result, equalTo(DONE));
  }
}
