package com.personal.beam;

import static com.personal.beam.Eloquent.createHistogram;
import static com.personal.beam.Eloquent.printToConsole;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;

import com.personal.beam.Helpers.GenericCoder;
import java.io.*;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import org.apache.beam.sdk.testing.PAssert;
import org.apache.beam.sdk.testing.TestPipeline;
import org.apache.beam.sdk.transforms.*;
import org.apache.beam.sdk.values.KV;
import org.apache.beam.sdk.values.PCollection;
import org.junit.*;

public class EloquentTest implements Serializable {
  private final String testString1 = "test";
  private final String testString2 = "goat";
  private final String outputFromMainFileRegex = "histogram\\.*";
  private final List<String> elems = asList(testString1, testString2, "");
  private final ClassLoader classLoader = getClass().getClassLoader();

  private File getFilePath(final String filePath) {
    return new File(Objects.requireNonNull(classLoader.getResource(filePath)).getFile());
  }

  @ClassRule
  public static TestPipeline p = TestPipeline.create().enableAbandonedNodeEnforcement(false);

  @Test
  public void testPrintToConsole() {
    final PCollection<String> output =
        p.apply("Printed Test Dataset", Create.of(elems))
            .apply("write out", printToConsole())
            .setCoder(new GenericCoder<>());
    PAssert.that(output).containsInAnyOrder(elems);
    p.run();
  }

  @Test
  public void testCreateHistogram() {
    final List<KV<String, Long>> expectedResult =
        singletonList(KV.of(Integer.toString(testString1.length()), 2L));
    final PCollection<KV<String, Long>> testHistogram =
        createHistogram(p.apply("Histogram Test Dataset", Create.of(elems)));
    PAssert.that(testHistogram).containsInAnyOrder(expectedResult);
    p.run();
  }

  @Test
  public void testCreateAndPrintHistogram() throws IOException {
    final PipelineHelper pipelineHelper = mock(PipelineHelper.class);
    when(pipelineHelper.get()).thenReturn(p);

    final List<String> expectedResult = singletonList(KV.of(testString1.length(), 2).toString());
    final String testFilePath = getFilePath("testFile.txt").getAbsolutePath();

    final Eloquent eloquent = new Eloquent();
    eloquent.filePath = testFilePath;

    eloquent.createAndPrintHistogram(pipelineHelper);

    final DirectoryStream<Path> histograms =
        Files.newDirectoryStream(new File(".").toPath(), outputFromMainFileRegex);
    final List<String> result = new ArrayList<>();
    histograms.forEach(
        path -> {
          try {
            result.addAll(Files.lines(path.toAbsolutePath()).collect(toList()));
          } catch (IOException e) {
            e.printStackTrace();
          }
        });

    assertThat(result.containsAll(expectedResult), is(true));
  }

  @Before
  public void setup() {
    p = TestPipeline.create().enableAbandonedNodeEnforcement(false);
  }

  @After
  public void tearDown() throws IOException {
    Files.newDirectoryStream(new File(".").toPath(), outputFromMainFileRegex)
        .forEach(file -> file.toFile().delete());
  }
}
