package com.personal.beam;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import java.io.Serializable;
import org.apache.beam.sdk.testing.TestPipeline;
import org.junit.ClassRule;
import org.junit.Test;

public class PipelineHelperTest implements Serializable {
  @ClassRule
  public static TestPipeline p = TestPipeline.create().enableAbandonedNodeEnforcement(false);

  private static final PipelineHelper pipelineHelper = new PipelineHelper();

  @Test
  public void testGet() {
    pipelineHelper.set(p);
    assertThat(pipelineHelper.get(), is(equalTo(p)));
  }
}
