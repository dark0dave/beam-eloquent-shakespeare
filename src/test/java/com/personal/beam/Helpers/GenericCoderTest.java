package com.personal.beam.Helpers;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.*;

import java.io.*;
import org.junit.Before;
import org.junit.Test;

public class GenericCoderTest {

  private GenericCoder<String> genericCoder;

  @Before
  public void setUp() {
    genericCoder = new GenericCoder<>();
  }

  @Test(expected = IllegalStateException.class)
  public void decode() {
    final InputStream input = new ByteArrayInputStream("".getBytes());
    genericCoder.decode(input);
  }

  @Test
  public void getCoderArguments() {
    assertThat(genericCoder.getCoderArguments(), equalTo(null));
  }

  @Test
  public void verifyDeterministic() {
    genericCoder.verifyDeterministic();
  }
}
